template "infrastructure-network.template" do
  project, role = name.split(%r{\.|-})
  source "network.json.erb"
  variables(
    :description => "Infrastructure Network Stack",
    :project => project,
    :role => role
  )
end

template "infrastructure-puppetmaster.template" do
  project, role = name.split(%r{\.|-})
  source "server.json.erb"
  variables(
    :description => "Infrastructure Puppetmaster Stack",
    :project => project,
    :role => role
  )
end

template "infrastructure-webserver.template" do
  project, role = name.split(%r{\.|-})
  source "server.json.erb"
  variables(
    :description => "Infrastructure Webserver Stack",
    :project => project,
    :role => role
  )
end
