# AWS-Cloudformation-Lono-Stacker

## What is AWS-CloudFormation-Lono-Stacker?

Lono is a Cloud Formation Template ruby generator. Lono generates Cloud Formation templates based on ERB templates. Stacker easily assemble CloudFormation stacks with interdependencies. This fork also contains custom templates.

## Requirements

AWS-CloudFormation-Lono-Stacker requires:

* Ruby 1.9.1+
* RubyGems 1.8+
* Bundler 1.3+

## Installation

Installing AWS-CloudFormation-Lono-Stacker can be done using the following commands to clone the repository and build the required gems.

	git clone https://bitbucket.org/mikelorant/ruby-aws-cloudformation-lono-stacker.git
	cd ruby-aws-cloudformation-lono-stacker
	bundle install

## Usage

The templates are automatically generated when the files change. This is handled by the Guard gem and this has been daemonised so it can run in the backgroumd. To start the daemon use the following command.

	./bin/god -c lono.god

The stacks can be assembled by using the following command.

	./bin/stacker update

You may need to set environmental variables for access to AWS.

	AWS_ACCESS_KEY_ID
	AWS_SECRET_ACCESS_KEY

## Troubleshoot

The logs are stored in the log directory. Every time a template is generated an entry should automatically be written. A good way to monitor the results is to use the follow feature of tail.

	tail -f log/lono.log
